from unittest import TestCase
from equipment.turtle_device import TurtleDevice


class TurtleDeviceTest(TestCase):

    def setUp(self) -> None:
        self.device = TurtleDevice()

    def test_open(self):
        self.device.open()
        self.device.execute('forward', 100)
        self.device.execute('left', 90)
        self.device.execute('right', 90)
        self.device.execute('color', 'red')
        self.device.execute('circle', 120, 180)
        self.device.execute('home')
        self.device.close()