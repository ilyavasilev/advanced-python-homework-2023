## advanced-python-homework-2023

## Installation

1. Install `Python`

2. Install `python3-venv` by the following command: `apt install python3-venv`

3. Create and activate your virtual environment following the [instructions](https://docs.python.org/3/tutorial/venv.html)

4. Install the `requirements.txt` dependencies following the commands:
```
cd ./scada_system
pip3 install -r requirements.txt
```

5. Install the package in editable mode:
```
pip3 install -e .
```

6. To generate documentation in HTML format, you need to run the following commands:
```
cd ./docs
make html
```
## Tests
1. To run the tests, go to the `scada_system` directory and run the following command:
```
python3 -m unittest
```