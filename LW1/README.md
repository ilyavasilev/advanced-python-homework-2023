# Results
## Function execution time measurement  
|Interpitator | Pure Python without type annotations | Pure Python with type annotations | Python + numpy|
| --- | --- | --- | --- |
|CPython 3.9 | 0.255727898 s | 0.250940687 s | 0.333585421 s|
|CPython 3.11 | 0.178182069 s | 0.18051334 s | 0.312025812 s |
|Latest PyPy for Python 3.9 | 0.039385466 s | 0.312025812 s | 1.51180428 s |
## Measuring the execution time of a function along with running the script and interpreter  
|Interpitator | Pure Python without type annotations | Pure Python with type annotations | Python + numpy | 
| --- | --- | --- | --- |
|CPython 3.9 | **real**: 0m0,260s; **user**: 0m0,260s; **sys**: 0m0,000s | **real**: 0m0,262s; **user**: 0m0,262s; **sys**: 0m0,000s | **real**: 0m0,458s; **user**: 0m0,644s; **sys**: 0m0,712s |
|CPython 3.11 | **real**: 0m0,191s; **user**: 0m0,191s; **sys**: 0m0,000s | **real**: 0m0,191s; **user**: 0m0,190s; **sys**: 0m0,000s | **real**: 0m0,426s; **user**: 0m0,533s; **sys**: 0m0,794s |
|Latest PyPy for Python 3.9 | **real**: 0m0,065s; **user**: 0m0,065s; **sys**: 0m0,000s | **real**: 0m0,064s; **user**: 0m0,064s; **sys**: 0m0,000s | **real**: 0m1,863s; **user**: 0m2,072s; **sys**: 0m0,687s |
